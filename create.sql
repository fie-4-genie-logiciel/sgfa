-- Instructions SQL pour recréer la base de données
-- Peuvent être générées automatiquement par hibernate
-- à partir des annotations sur les entités

create sequence hibernate_sequence start with 1 increment by 1

    create table employee (
       id integer not null,
        habilite_pro boolean not null,
        name varchar(255),
        primary key (id)
    )

    create table service_request (
       id integer not null,
        debut_demande timestamp,
        debut_service timestamp,
        fin_service timestamp,
        numero integer not null,
        type varchar(255),
        servie_pendant_id integer,
        primary key (id)
    )

    create table work_session (
       id integer not null,
        fermeture timestamp,
        numero_guichet integer,
        ouverture timestamp,
        ouvert_par_id integer,
        primary key (id)
    )

    alter table service_request 
       add constraint FKjwoghxbu77b2khv723cbt9vxv 
       foreign key (servie_pendant_id) 
       references work_session

    alter table work_session 
       add constraint FKhqj0rng5pn97m8un4vffemia4 
       foreign key (ouvert_par_id) 
       references employee

-- Pour faciliter l'écriture des requêtes SQL des statistiques
DROP ALIAS IF EXISTS trancheHoraire;
CREATE ALIAS trancheHoraire FOR "io.github.bastide.sgfa.sqlutil.SqlFunctions.trancheHoraire";

-- Exemple de requête SQL pour les stats d'affluence au guichet
SELECT 
      DAYNAME(DEBUT_SERVICE) AS JOUR,
      -- fonction implémentée en Java (cf. sqlutil)
      trancheHoraire(DEBUT_SERVICE) as TRANCHE, 
     TYPE,  
     COUNT(*) AS Nombre, 
     MIN( SECOND( DEBUT_SERVICE - DEBUT_DEMANDE ) ) AS ATTENTE_MINIMUM_EN_SECONDES, 
     MAX( SECOND( DEBUT_SERVICE - DEBUT_DEMANDE ) ) AS ATTENTE_MAXIMUM_EN_SECONDES
FROM SERVICE_REQUEST 
GROUP BY JOUR, TRANCHE, TYPE