package io.github.bastide.sgfa.components;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import io.github.bastide.sgfa.dao.EmployeeRepository;
import io.github.bastide.sgfa.dao.RequestRepository;
import io.github.bastide.sgfa.dao.WorkSessionRepository;
import io.github.bastide.sgfa.entity.RequestType;
import io.github.bastide.sgfa.exception.SGFAException;

@SpringBootTest
class SGFATest {
    @MockBean
    private DeskManager deskManager;

    @MockBean
    Dispatcher dispatcher;

    @MockBean
    RequestQueue queue;

    @MockBean
    EmployeeRepository employeeRepository;

    @MockBean
    RequestRepository requestRepository;

    @MockBean
    WorkSessionRepository workSessionRepository;

    @MockBean
    SimpMessagingTemplate simpMessagingTemplate;

    SGFA sgfa;

    @BeforeEach
    public void setUp() {
        sgfa = new SGFA(queue, dispatcher, deskManager, requestRepository, employeeRepository, workSessionRepository,
                simpMessagingTemplate);
    }

    @Test
    void neDelivrePasDeTicketSiRequeteProEtPasDEmployePro() {
        // Given : un SGFA
        // When : il n'y a pas d'employé pro connecté
        Mockito.when(deskManager.existeEmployeProConnecte()).thenReturn(false);
        // Then : on ne doit pas pouvoir délivrer de ticket
        Exception received = assertThrows(SGFAException.class, () -> sgfa.genereTicket(RequestType.GUICHET_PRO));
        assertEquals("Aucun employé 'pro' connecté", received.getMessage());
    }
}
