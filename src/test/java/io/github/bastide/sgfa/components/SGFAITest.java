package io.github.bastide.sgfa.components;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import io.github.bastide.sgfa.entity.RequestType;
import lombok.extern.slf4j.Slf4j;

@SpringBootTest
@AutoConfigureMockMvc
@Slf4j
/**
 * Un test d'intégration. On teste l'application avec tous ses composants, en
 * appelant les contrôleurs Spring par leur URL.
 */
class SGFAITest {
    @Autowired
    // Permet de simuler les requêtes HTTP reçues par les contrôleurs
    private MockMvc mvc;

    @Test
    void lUrlRacineAfficheIndexPointHtml() throws Exception {
        log.info("Test du contrôleur racine");
        mvc.perform(get("/")).andExpect(forwardedUrl("index.html")).andExpect(status().isOk());
    }

    @Test
    void onPeutImprimerUnTicketNonPro() throws Exception {
        log.info("Test d'impression d'un ticket non pro'");
        RequestType type = RequestType.TOUTES_OPERATIONS;
        mvc.perform(get("/ticket/{type}", type)).andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(status().isOk());
    }

    @Test
    void useCaseAccederAuGuichet() throws Exception {
        log.info("Use Case 'Accéder au Guichet'");
        RequestType type = RequestType.TOUTES_OPERATIONS;
        int employeeNumber = 1;
        int deskNumber = 3;
        // Given : un SGFA
        // When :
        // Un employé quelconque se connecte à un guichet
        mvc.perform(get("/authentification/login?employeeNumber={employeeNumber}&deskNumber={deskNumber}",
                employeeNumber, deskNumber)).andExpect(status().isOk());
        // Un client imprime un ticket non pro
        mvc.perform(get("/ticket/{type}", type)).andExpect(status().isOk());
        // L'employé devient disponible à ce guichet
        mvc.perform(get("/employee/available?deskNumber={deskNumber}", deskNumber)).andExpect(status().isOk());
        // Then : On doit voir le numéro du guichet sur l'écran
        MvcResult result = mvc.perform(get("/display")).andExpect(status().isOk()).andReturn();
        // On examine le HTML reçu en réponse
        String html = result.getResponse().getContentAsString();
        log.info("Réponse reçue {}", html);
        assertTrue(html.contains("<span>" + deskNumber + "</span>"),
                "Le numéro du guichet doit être visible sur l'écran");
    }
}
