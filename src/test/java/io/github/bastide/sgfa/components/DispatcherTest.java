package io.github.bastide.sgfa.components;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import io.github.bastide.sgfa.entity.Employee;
import io.github.bastide.sgfa.entity.RequestType;
import io.github.bastide.sgfa.entity.ServiceRequest;
import io.github.bastide.sgfa.entity.WorkSession;

@SpringBootTest
/**
 * Test unitaire de la classe Dispatcher.
 * Utilise des 'mocks' Mockito pour simuler les autres composants.
 * @See https://www.baeldung.com/java-spring-mockito-mock-mockbean
 */
class DispatcherTest {
    private static final int NUM_REQUETE_PRO = 1000;
    private static final int NUM_REQUETE_NON_PRO = 1;
    private static final int NUM_GUICHET_PRO = 1;
    private static final int NUM_GUICHET_NON_PRO = 2;

    // Les dépendances, qui sont 'mockées'
    @MockBean
    RequestQueue requestQueue;

    @MockBean
    DeskManager deskManager;

    // L'objet à tester
    Dispatcher dispatcher;

    ServiceRequest requeteNonPro;
    ServiceRequest requetePro;
    Employee employePro;
    Employee employeNonPro;

    WorkSession sessionPro;
    WorkSession sessionNonPro;

    @BeforeEach
    public void setUp() {
        employePro = new Employee("Un employé pro", true);
        employeNonPro = new Employee("Un employé non pro", false);
        requeteNonPro = new ServiceRequest(NUM_REQUETE_NON_PRO, RequestType.TOUTES_OPERATIONS);
        requetePro = new ServiceRequest(NUM_REQUETE_PRO, RequestType.GUICHET_PRO);
        sessionPro = new WorkSession(NUM_GUICHET_PRO, employePro);
        sessionNonPro = new WorkSession(NUM_GUICHET_NON_PRO, employeNonPro);

        // On crée l'objet à tester
        dispatcher = new Dispatcher(requestQueue, deskManager);
    }

    @Test
    void lesRequetesProSontPrioritaires() {
        // Given
        // Il y a un employé non pro connecté
        Mockito.when(deskManager.guichetPourServiceNonPro()).thenReturn(Optional.of(sessionNonPro));
        // Il y a aussi un employé non pro connecté
        Mockito.when(deskManager.guichetPourServicePro()).thenReturn(Optional.of(sessionPro));
        // Il y a une requête non pro en attente
        Mockito.when(requestQueue.requeteServiceNonPro()).thenReturn(Optional.of(requeteNonPro));
        // Il y a aussi une requête pro en attente
        Mockito.when(requestQueue.requeteServicePro()).thenReturn(Optional.of(requetePro));
        // When
        // On essaie d'envoyer une requête au guichet
        Optional<ServiceRequest> result = dispatcher.envoieRequeteAuGuichet();
        // Then
        // On peut servir une requête
        assertTrue(result.isPresent());
        ServiceRequest requete = result.get();
        // C'est une requête pro
        assertEquals(RequestType.GUICHET_PRO, requete.getType());
    }

}
