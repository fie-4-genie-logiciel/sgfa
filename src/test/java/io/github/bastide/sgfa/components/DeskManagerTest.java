package io.github.bastide.sgfa.components;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.github.bastide.sgfa.entity.Employee;

class DeskManagerTest {
    // Quelques objets utiles aux tests
    Employee employePro;
    Employee employeNonPro;
    // L'objet à tester
    DeskManager deskManager;

    @BeforeEach
    public void setUp() {
        // Initialisation des objets utiles aux tests
        employePro = new Employee("Un employé pro", true);
        employeNonPro = new Employee("Un employé non pro", false);
        // On crée un nouveau DeskManager vide
        deskManager = new DeskManager();
    }

    @Test
    void deskManagerCorrectementInitialise() {
        // Given : un deskManager tout neuf
        // Then : il est vide
        assertFalse(deskManager.existeEmployeProConnecte());
        assertTrue(deskManager.guichetPourServicePro().isEmpty());
        assertTrue(deskManager.guichetPourServiceNonPro().isEmpty());
    }

    @Test
    void unProPeutServirDesRequetesNonPro() throws Exception {
        // Given : un deskManager vide
        // When : Un employé pro se connecte à un guichet
        int deskNumber = 1;
        deskManager.employeConnecte(employePro, deskNumber);
        // et l'employé devient disponible à ce guichet
        deskManager.employeDisponible(deskNumber);
        // Then : On peut servir une requête non pro
        assertTrue(deskManager.guichetPourServiceNonPro().isPresent());
    }

    @Test
    void unNonProNePeutPasServirDesRequetesPro() throws Exception {
        // Given : un deskManager vide
        // When : Un employé non pro se connecte à un guichet
        int deskNumber = 1;
        deskManager.employeConnecte(employeNonPro, deskNumber);
        // et l'employé devient disponible à ce guichet
        deskManager.employeDisponible(deskNumber);
        // Then : On ne peut pas servir une requête pro
        assertTrue(deskManager.guichetPourServicePro().isEmpty());
    }
}
