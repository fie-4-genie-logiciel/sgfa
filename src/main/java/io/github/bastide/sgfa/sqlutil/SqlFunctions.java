package io.github.bastide.sgfa.sqlutil;

import java.sql.Timestamp;

/**
 * Quelques fonctions utiles pour les requêtes SQL.
 */
public class SqlFunctions {
    private SqlFunctions() {
    }

    /**
     * @param heureRequete l'heure de début de service de la requête
     * @return la tranche horaire de la journée
     */
    public static String trancheHoraire(Timestamp heureRequete) {
        switch (heureRequete.toLocalDateTime().getHour()) {
        case 8, 9:
            return "Début de matinée";
        case 10:
            return "Milieu de matinée";
        case 11, 12:
            return "Fin de matinée";
        case 13, 14:
            return "Début d'après midi";
        case 15, 16:
            return "Milieu d'après midi";
        case 17, 18:
            return "Fin d'après-midi";
        default:
            return "Hors période d'ouverture";
        }
    }
}
