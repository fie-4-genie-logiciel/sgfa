package io.github.bastide.sgfa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SgfaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SgfaApplication.class, args);
	}

}
