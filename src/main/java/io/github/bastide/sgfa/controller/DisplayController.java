package io.github.bastide.sgfa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import io.github.bastide.sgfa.components.IDisplayAPI;

@Controller
public class DisplayController {
    /**
     * On pourrait utiliser directement SGFA mais il vaut mieux dépendre uniquement
     * de l'API d'affichage (minimise les dépendances, facilite le test)
     */
    private final IDisplayAPI displayAPI;
    // Injection de dépendance
    // Le composant nécessaire (SGFA) sera "injecté" par Spring
    public DisplayController(IDisplayAPI displayAPI) {
        this.displayAPI = displayAPI;
    }

    @GetMapping("/display")
    public String show(Model model) {
        model.addAttribute("enCours", displayAPI.requetesEnCours());
        return "display";
    }
}
