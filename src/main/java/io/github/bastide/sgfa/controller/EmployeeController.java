package io.github.bastide.sgfa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import io.github.bastide.sgfa.components.IEmployeeAPI;

@Controller
public class EmployeeController {
    /**
     * On pourrait utiliser directement SGFA mais il vaut mieux dépendre uniquement
     * de l'API des employés (minimise les dépendances, facilite le test)
     */
    private final IEmployeeAPI employeeAPI;
    // Injection de dépendance
    // Le composant nécessaire (SGFA) sera "injecté" par Spring
    public EmployeeController(IEmployeeAPI employeeAPI) {
        this.employeeAPI = employeeAPI;
    }

    @GetMapping("/employee/available")
    public @ResponseBody String available(int deskNumber) {
        employeeAPI.employeDisponible(deskNumber);
        return "OK";
    }

    @GetMapping("/employee/busy")
    public @ResponseBody String busy(int deskNumber) {
        employeeAPI.employeIndisponible(deskNumber);
        return "OK";
    }

}
