package io.github.bastide.sgfa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import io.github.bastide.sgfa.Ticket;
import io.github.bastide.sgfa.components.ITicketAPI;
import io.github.bastide.sgfa.entity.RequestType;

@Controller
public class TicketController {
    /**
     * On pourrait utiliser directement SGFA mais il vaut mieux dépendre uniquement
     * de l'API nécessaire à la borne (minimise les dépendances, facilite le test)
     */
    private final ITicketAPI ticketAPI;
    // Injection de dépendance
    // Le composant nécessaire (SGFA) sera "injecté" par Spring
    public TicketController(ITicketAPI ticketAPI) {
        this.ticketAPI = ticketAPI;
    }

    @GetMapping("/ticket/{type}")
    public String printTicketFor(@PathVariable("type") RequestType type, Model model) {
        String view;
        try {
            Ticket result = ticketAPI.genereTicket(type);
            model.addAttribute("ticket", result);
            view = "ticket"; // On affiche le ticket normal
        } catch (Exception e) {
            view = "errorTicket"; // On affiche le ticket d'erreur
        }
        return view;
    }
}
