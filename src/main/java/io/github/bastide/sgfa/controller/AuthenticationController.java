package io.github.bastide.sgfa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.extern.slf4j.Slf4j;
import io.github.bastide.sgfa.components.IAuthenticationAPI;
import io.github.bastide.sgfa.entity.WorkSession;

@Controller
@Slf4j // Logger
@RequestMapping(path = "/authentification")
public class AuthenticationController {
    /**
     * On pourrait utiliser directement SGFA mais il vaut mieux dépendre uniquement
     * de l'API d'authentification (minimise les dépendances, facilite le test)
     */
    private final IAuthenticationAPI authApi;
    // Injection de dépendance
    // Le composant nécessaire (SGFA) sera "injecté" par Spring
    public AuthenticationController(IAuthenticationAPI authApi) {
        this.authApi = authApi;
    }

    @GetMapping("login")
    public String login(int employeeNumber, int deskNumber, Model model) {
        WorkSession session = null;
        try {
            session = authApi.employeConnecte(employeeNumber, deskNumber);
        } catch (Exception e) {
            log.error("Erreur lors de la connexion de l'employé {} sur le guichet {}", employeeNumber, deskNumber, e);
        }
        model.addAttribute("guichet", session);
        return "employeeUI";
    }

    @GetMapping("logout")
    public @ResponseBody String logout(int deskNumber) {
        authApi.employeDeconnecte(deskNumber);
        return "Good Bye !";
    }

}
