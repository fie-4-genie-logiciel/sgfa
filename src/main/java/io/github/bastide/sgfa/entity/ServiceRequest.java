package io.github.bastide.sgfa.entity;

import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

import org.springframework.data.jpa.domain.AbstractPersistable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Une demande de service
 */
@Entity
// Lombok
@Getter
@Setter
@ToString
public class ServiceRequest extends AbstractPersistable<Integer> implements Serializable {

    public ServiceRequest() {
        this(0, RequestType.TOUTES_OPERATIONS);
    }

    public ServiceRequest(int numero, RequestType type) {
        this.numero = numero;
        this.type = type;
    }

    /**
     * Le numéro n'est pas la clé, les numéros sont réinitialisés tous les jours
     *
     */
    private final int numero;

    @Enumerated(value = EnumType.STRING)
    private final RequestType type;

    private final LocalDateTime debutDemande = LocalDateTime.now();

    // Renseignés au début du service de cette RequeteDeService
    private LocalDateTime debutService = null;

    // Renseignés à la fin du service de cette RequeteDeService
    private LocalDateTime finService = null;

    @ManyToOne
    private WorkSession serviePendant = null;

    /**
     * Enregistre le début du service pour cette RequeteDeService
     *
     * @param session la session de travail pendant laquelle cette RequeteDeService
     *                est servie
     */
    public void demarreService(WorkSession session) {
        this.serviePendant = session;
        this.debutService = LocalDateTime.now();
    }

    /**
     * Enreigistre la fin de service de cette RequeteDeService
     */
    public void termineService() {
        this.finService = LocalDateTime.now();
    }

    public Duration tempsDAttente() {
        return Duration.between(debutDemande, debutService);
    }

    public Duration tempsDeService() {
        return Duration.between(debutService, finService);
    }

    public Integer getGuichetAffecte() {
        return serviePendant.getNumeroGuichet();
    }
}
