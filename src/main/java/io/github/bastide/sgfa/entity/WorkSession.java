package io.github.bastide.sgfa.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.springframework.data.jpa.domain.AbstractPersistable;

import lombok.*;

@Entity
// Lombok
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
public class WorkSession extends AbstractPersistable<Integer> implements Serializable {

    @NonNull
    @Setter(AccessLevel.NONE)
    private Integer numeroGuichet;

    @ManyToOne
    @NonNull
    @Setter(AccessLevel.NONE)
    private Employee ouvertPar;

    private final LocalDateTime ouverture = LocalDateTime.now();

    @Setter(AccessLevel.NONE)
    private LocalDateTime fermeture = null;

    @Transient // Non sauvegardé dans la base de données
    private boolean employeDisponible = false;

    public void fermer() {
        fermeture = LocalDateTime.now();
    }

    public boolean isEmployeHabilitePro() {
        return ouvertPar.isHabilitePro();
    }
}
