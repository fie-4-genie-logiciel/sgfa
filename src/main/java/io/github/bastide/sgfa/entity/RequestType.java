package io.github.bastide.sgfa.entity;

public enum RequestType {
	GUICHET_PRO, RETRAIT_INSTANCES, TOUTES_OPERATIONS
}
