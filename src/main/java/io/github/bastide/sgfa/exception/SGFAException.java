package io.github.bastide.sgfa.exception;

public class SGFAException extends Exception {
    public SGFAException(String message) {
        super(message);
    }
}