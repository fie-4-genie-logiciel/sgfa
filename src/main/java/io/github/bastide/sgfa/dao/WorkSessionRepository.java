package io.github.bastide.sgfa.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import io.github.bastide.sgfa.entity.WorkSession;

// This will be AUTO IMPLEMENTED by Spring into a Bean 
public interface WorkSessionRepository extends JpaRepository<WorkSession, Integer> {
}