package io.github.bastide.sgfa.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import io.github.bastide.sgfa.entity.Employee;


// This will be AUTO IMPLEMENTED by Spring into a Bean 
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
}
