package io.github.bastide.sgfa.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import io.github.bastide.sgfa.entity.ServiceRequest;

// This will be AUTO IMPLEMENTED by Spring into a Bean 
public interface RequestRepository extends JpaRepository<ServiceRequest, Integer> {
}
