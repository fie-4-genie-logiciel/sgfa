package io.github.bastide.sgfa;

import io.github.bastide.sgfa.entity.RequestType;

public class Ticket {
    public final int numeroAppel;
    public final RequestType type;

    public Ticket(int numeroAppel, RequestType type) {
        this.numeroAppel = numeroAppel;
        this.type = type;
    }

}
