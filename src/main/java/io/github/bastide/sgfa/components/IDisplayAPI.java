package io.github.bastide.sgfa.components;

import java.util.Collection;

import io.github.bastide.sgfa.entity.ServiceRequest;

public interface IDisplayAPI {
    /**
     * @return les requêtes en cours de service
     */
    public Collection<ServiceRequest> requetesEnCours();
}
