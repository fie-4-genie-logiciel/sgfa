package io.github.bastide.sgfa.components;

import io.github.bastide.sgfa.Ticket;
import io.github.bastide.sgfa.entity.RequestType;

/**
 * L'API utilisée par le controleur Bull X379
 */
public interface ITicketAPI {
    /**
     * @param type l'opération demandée
     * @return les infos nécessaires à l'impression du ticket
     * @throws Exception si aucun employé n'est disponible pour l'opération demandée
     */
    Ticket genereTicket(RequestType type) throws Exception;
}
