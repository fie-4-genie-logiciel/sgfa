package io.github.bastide.sgfa.components;

import io.github.bastide.sgfa.entity.WorkSession;

public interface IAuthenticationAPI {
  /**
   * Enregistre la connexion d'un employé au guichet donné
   * 
   * @param employeeNumber le numéro de l'employé
   * @param deskNumber     le numéro du guichet
   * @return la nouvelle session de travail
   * @throws Exception si les paramètres sont invalides
   */
  public WorkSession employeConnecte(int employeeNumber, int deskNumber) throws Exception;

  /**
   * Enregistre la déconnexion de l'employé au guichet donné
   * 
   * @param deskNumber le numéro du guichet
   */
  public void employeDeconnecte(int deskNumber);
}
