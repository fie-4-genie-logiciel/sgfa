package io.github.bastide.sgfa.components;

import java.util.Optional;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.ApplicationScope;

import io.github.bastide.sgfa.entity.ServiceRequest;
import io.github.bastide.sgfa.entity.WorkSession;
import lombok.extern.slf4j.Slf4j;

@Slf4j

@Component
@ApplicationScope
public class Dispatcher {
	private final RequestQueue requestQueue;
	private final DeskManager deskManager;

	/**
	 * @param requestQueue la file d'attente (injectée)
	 * @param deskManager  le routeur (injecté)
	 */
	public Dispatcher(RequestQueue requestQueue, DeskManager deskManager) {
		this.requestQueue = requestQueue;
		this.deskManager = deskManager;
	}

	/**
	 * Essaie d'envoyer une requête en attente susceptible de être traitée en
	 * priorisant les requêtes "pro"
	 * 
	 * @return la requête la plus prioritaire susceptible d'être servie, si possible
	 */
	synchronized Optional<ServiceRequest> envoieRequeteAuGuichet() {
		try {
			// On essaie d'abord une requête pro
			log.info("Est-ce qu'on peut servir une requête pro ?");
			WorkSession session = deskManager.guichetPourServicePro()
					.orElseThrow(() -> new Exception("Pas de guichet pro disponible"));
			ServiceRequest request = requestQueue.requeteServicePro()
					.orElseThrow(() -> new Exception("Pas de requête pro en attente"));
			// On a une requête pro en attente, et un guichet pro disponible
			request.demarreService(session);
			return Optional.of(request);
		} catch (Exception ex) {
			log.info(ex.getMessage());
		}

		try { // On essaie ensuite une requête normale
			log.info("Est-ce qu'on peut servir une requête non pro ?");
			WorkSession desk = deskManager.guichetPourServiceNonPro()
					.orElseThrow(() -> new Exception("Pas de guichet non pro disponible"));
			ServiceRequest request = requestQueue.requeteServiceNonPro()
					.orElseThrow(() -> new Exception("Pas de requête non pro en attente"));
			// On a une requête no-pro en attente, et un guichet quelconque disponible
			request.demarreService(desk);
			return Optional.of(request);
		} catch (Exception ex) {
			log.info(ex.getMessage());
		}
		// Impossible de servir une requête en ce moment
		return Optional.empty();
	}

}
