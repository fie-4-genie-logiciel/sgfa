package io.github.bastide.sgfa.components;

public interface IEmployeeAPI {
    /**
     * Enregistre la disponibilité de l'employé connecté au guichet donné
     * 
     * @param deskNumber le numéro du guichet où l'employé est connecté
     */
    public void employeDisponible(int deskNumber);

    /**
     * Enregistre l'indisponibilité de l'employé connecté au guichet donné
     * 
     * @param deskNumber le numéro du guichet où l'employé est connecté
     */
    public void employeIndisponible(int deskNumber);
}