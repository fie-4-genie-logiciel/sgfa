package io.github.bastide.sgfa.components;

import java.util.Map;
import java.util.Optional;
import java.util.HashMap;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.ApplicationScope;

import io.github.bastide.sgfa.entity.Employee;
import io.github.bastide.sgfa.entity.WorkSession;
import lombok.extern.slf4j.Slf4j;

@Component
@ApplicationScope

@Slf4j
public class DeskManager {
	private final Map<Integer, WorkSession> enCours = new HashMap<>();

	/**
	 * Enregistre la connexion d'un employé à son guichet, crée une nouvelle session
	 * de travail
	 * 
	 * @param employee   l'employé qui vient de se connecter
	 * @param deskNumber le numéro du guichet
	 * @return la session de travail
	 */
	public WorkSession employeConnecte(Employee employee, int deskNumber) throws Exception {
		// Normalement ce test devrait être fait par le système d'authentification
		if (estConnecte(employee) || estOccupe(deskNumber))
			throw new Exception("Employé déjà connecté ou guichet déjà occupé");

		log.info("L'employé {} vient de se connecter au guichet {}", employee.getName(), deskNumber);

		WorkSession nouvelle = new WorkSession(deskNumber, employee);
		enCours.put(deskNumber, nouvelle);
		return nouvelle;
	}

	/**
	 * Enregistre la déconnexion d'un employé de son guichet
	 * 
	 * @param deskNumber le numéro du guichet
	 * @return la session de travail si elle existe
	 */
	public Optional<WorkSession> employeDeconnecte(int deskNumber) {
		log.info("L'employé vient de se déconnecter du guichet {}", deskNumber);
		return Optional.of(enCours.remove(deskNumber));
	}

	/**
	 * Enregistre la disponibilité d'un employé connecté
	 * 
	 * @param deskNumber le numéro du guichet
	 */
	public void employeDisponible(int deskNumber) {
		WorkSession guichet = enCours.get(deskNumber);
		guichet.setEmployeDisponible(true);
		log.info("L'employé {} est disponible au guichet {}", guichet.getOuvertPar().getName(), deskNumber);
	}

	/**
	 * Enregistre l'indisponibilité d'un employé connecté
	 * 
	 * @param deskNumber le numéro du guichet
	 */
	public void employeIndisponible(int deskNumber) {
		WorkSession guichet = enCours.get(deskNumber);
		guichet.setEmployeDisponible(false);
		log.info("L'employé {} est indisponible au guichet {}", guichet.getOuvertPar().getName(), deskNumber);
	}

	/**
	 * Détermine si on peut servir une requête "non pro" sur une des sessions de
	 * travail en cours (il faut que l'employé soit disponible)
	 *
	 * @return une session de travail appropriée, si possible
	 */
	Optional<WorkSession> guichetPourServiceNonPro() {
		for (WorkSession session : enCours.values()) // Pour chaque session de travail en cours
			if (session.isEmployeDisponible()) // Si l'employé connecté est disponible
				return Optional.of(session); // On renvoie la session
		// On n'a trouvé aucun employé disponible
		return Optional.empty();
		// ça peut s'écrire en une seule ligne en utilisant les 'stream'
		// @See https://www.tutorialspoint.com/java8/java8_streams.htm
		// return enCours.values().stream().filter( WorkSession::isEmployeDisponible.findAny();
	}

	/**
	 * Détermine si on peut servir une requête "pro" sur une des sessions de travail
	 * en cours (il faut que l'employé soit habilité et disponible)
	 * 
	 * @return une session de travail appropriée, si possible
	 */
	Optional<WorkSession> guichetPourServicePro() {
		return enCours.values().stream().filter(WorkSession::isEmployeHabilitePro).filter(WorkSession::isEmployeDisponible).findAny();
	}

	/**
	 * @return true si il y a au moins un employé habilité pro connecté
	 */
	public boolean existeEmployeProConnecte() {
		// y-a-t'il une session en cours pour laquelle la méthode isEmployeHabilitePro
		// renvoie vrai ?
		return enCours.values().stream().anyMatch(WorkSession::isEmployeHabilitePro);
	}

	private boolean estConnecte(Employee e) {
		return enCours.values().stream().anyMatch(session -> (session.getOuvertPar() == e));
	}

	private boolean estOccupe(int deskNumber) {
		return enCours.containsKey(deskNumber);
	}
}
