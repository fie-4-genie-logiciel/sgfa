
package io.github.bastide.sgfa.components;

import java.util.LinkedList;
import java.util.Optional;
import java.util.Queue;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.ApplicationScope;

import io.github.bastide.sgfa.entity.RequestType;
import io.github.bastide.sgfa.entity.ServiceRequest;

@Component //Un composant géré par Spring
@ApplicationScope // Un composant qui est unique pour l'application (singleton)
/** une double file d'attente pour les requêtes */
public class RequestQueue {
	// Les requêtes pro son numérotées à partir de 1000
	public static final int FIRST_PRO_NUMBER = 1000;
	public static final int FIRST_NONPRO_NUMBER = 0;
	// Le numéro de requête, incrémenté à chaque nouvelle requête
	private int proRequestNumber = FIRST_PRO_NUMBER;
	private int nonProRequestNumber = FIRST_NONPRO_NUMBER;

	// Les deux files d'attente (interface / implémentation)
	private final Queue<ServiceRequest> pro = new LinkedList<>();
	private final Queue<ServiceRequest> nonPro = new LinkedList<>();

	/**
	 * Crée une nouvelle requête, lui attribue un numéro, et l'ajoute à la file
	 * d'attente appropriée
	 * 
	 * @param type le type de la requête
	 * @return la nouvelle requête
	 */
	public synchronized ServiceRequest nouvelleRequete(RequestType type) {
		ServiceRequest result;
		// Numérotation des requêtes en fonction de leur type
		if (type == RequestType.GUICHET_PRO) {
			result = new ServiceRequest(++proRequestNumber, type);
			pro.add(result);
		} else {
			result = new ServiceRequest(++nonProRequestNumber, type);
			nonPro.add(result);
		}
		return result;
	}

	/**
	 * @return la requête "pro" la plus prioritaire, si il y en a une
	 */
	public Optional<ServiceRequest> requeteServicePro() {
		return getFirstInQueue(pro);
	}

	/**
	 * @return la requête "non pro" la plus prioritaire, si il y en a une
	 */
	public Optional<ServiceRequest> requeteServiceNonPro() {
		return getFirstInQueue(nonPro);
	}

	private Optional<ServiceRequest> getFirstInQueue(Queue<ServiceRequest> queue) {
		return Optional.of(queue.poll());
	}
}
