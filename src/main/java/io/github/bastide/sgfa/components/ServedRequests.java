package io.github.bastide.sgfa.components;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import io.github.bastide.sgfa.entity.ServiceRequest;

public class ServedRequests {
    // Clé : le numéro du guichet, Valeur : une requête en cours de service sur ce guichet
    private Map<Integer, ServiceRequest> enCours = new HashMap<>();

    /**
     * Une nouvelle requête est ajoutée à la liste des requêtes en cours de
     * traitement.
     * 
     * @param request la requête à ajouter
     */
    public void ajoute(ServiceRequest request) {
        int numeroGuichet = request.getGuichetAffecte();
        enCours.put(numeroGuichet, request);
    }

    /**
     * Termine la requête en cours de traitement sur le guichet donné.
     * 
     * @param deskNumber le numéro du guichet
     * @return la requête dont le traitement vient de se terminer (optionnel)
     */
    public Optional<ServiceRequest> termine(int deskNumber) {
        return Optional.ofNullable(enCours.remove(deskNumber));
    }

    /**
     * @return la collection des requêtes en cours de traitement.
     */
    public Collection<ServiceRequest> requetesEnCours() {
        return enCours.values();
    }
}
