package io.github.bastide.sgfa.components;

import java.util.Collection;
import java.util.Optional;

import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.ApplicationScope;

import io.github.bastide.sgfa.Ticket;
import io.github.bastide.sgfa.dao.EmployeeRepository;
import io.github.bastide.sgfa.dao.RequestRepository;
import io.github.bastide.sgfa.dao.WorkSessionRepository;
import io.github.bastide.sgfa.entity.Employee;
import io.github.bastide.sgfa.entity.RequestType;
import io.github.bastide.sgfa.entity.ServiceRequest;
import io.github.bastide.sgfa.entity.WorkSession;
import io.github.bastide.sgfa.exception.SGFAException;
import lombok.extern.slf4j.Slf4j;

@Component
@ApplicationScope
@Slf4j
/*
  Implémente les API nécessaires aux différents contrôleurs Gère la persistance
  des données Protège des accès concurrents (synchronized)
 */
public class SGFA implements ITicketAPI, IEmployeeAPI, IAuthenticationAPI, IDisplayAPI {
    private final RequestQueue requestQueue;
    private final Dispatcher dispatcher;
    private final DeskManager deskManager;
    private final RequestRepository requestDao;
    private final EmployeeRepository employeeDao;
    private final WorkSessionRepository workSessionDAO;
    private final SimpMessagingTemplate publisher;

    private final ServedRequests enCours = new ServedRequests();

    /**
     * Les paramètres (qui sont d'autres composants gérés par Spring) seront
     * "injectés" automatiquement à la construction
     * 
     * TODO: trop de dépendances sur les DAO, faire une façade !
     * 
     * @param requestDao     DAO des requêtes
     * @param employeeDao    DAO des employés
     * @param workSessionDAO DAO des sessions de travail
     * @param requestQueue   la file d'attente des requêtes
     * @param dispatcher     le routeur
     * @param deskManager    le gestionnaire de sessions de travail
     * @param publisher      canal de diffusion webSockets
     */
    public SGFA(RequestQueue requestQueue, Dispatcher dispatcher, DeskManager deskManager, RequestRepository requestDao,
            EmployeeRepository employeeDao, WorkSessionRepository workSessionDAO, SimpMessagingTemplate publisher) {
        this.requestQueue = requestQueue;
        this.dispatcher = dispatcher;
        this.deskManager = deskManager;
        this.requestDao = requestDao;
        this.employeeDao = employeeDao;
        this.workSessionDAO = workSessionDAO;
        this.publisher = publisher;
    }

    /**
     * Enregistre une nouvelle requête dans la file d'attente, et essaie de la
     * router vers un guichet approprié.
     * 
     * @param type l'opération demandée
     * @return les infos nécessaires à l'impression du ticket
     */
    public synchronized Ticket genereTicket(RequestType type) throws Exception {
        // On vérifie qu'il est possible de servir une requête 'pro'
        if (type.equals(RequestType.GUICHET_PRO) && !deskManager.existeEmployeProConnecte()) {
            throw new SGFAException("Aucun employé 'pro' connecté");
        }
        ServiceRequest nouvelle = requestQueue.nouvelleRequete(type);
        log.info("On génère une nouvelle demande de service : {}", nouvelle);
        dispatchRequest();
        return new Ticket(nouvelle.getNumero(), nouvelle.getType());
    }

    /**
     * Enregistre la disponibilité de l'employé connecté au guichet donné
     * 
     * @param deskNumber le numéro du guichet où l'employé est connecté
     */
    public synchronized void employeDisponible(int deskNumber) {
        termineRequete(deskNumber); // Si une requête est en cours, la terminer
        deskManager.employeDisponible(deskNumber); // On indique à deskManager que l'employé est disponible
        // Est-ce qu'on peut envoyer un client à cet employé ?
        dispatchRequest();
    }

    /**
     * Enregistre l'indisponibilité de l'employé connecté au guichet donné
     * 
     * @param deskNumber le numéro du guichet où l'employé est connecté
     */
    public synchronized void employeIndisponible(int deskNumber) {
        termineRequete(deskNumber); // Si une requête est en cours, la terminer
        deskManager.employeIndisponible(deskNumber);
    }

    /**
     * Enregistre la connexion d'un employé au guichet donné, Démarre une session de
     * travail pour cet employé
     * 
     * @param employeeNumber le numéro de l'employé
     * @param deskNumber     le numéro du guichet
     * @return la nouvelle session de travail
     * @throws Exception si les paramètres sont invalides
     */
    public synchronized WorkSession employeConnecte(int employeeNumber, int deskNumber) throws Exception {
        Employee e = employeeDao.findById(employeeNumber).orElseThrow();
        WorkSession nouvelle = deskManager.employeConnecte(e, deskNumber);
        // On la persiste pour avoir sa clé auto générée,
        // nécessaire pour pouvoir enregistrer des ServiceRequest dans cette WorkSession
        workSessionDAO.save(nouvelle);
        log.info("On enregistre l'ouverture du guichet : {}", nouvelle);
        return nouvelle;
    }

    /**
     * Enregistre la déconnexion de l'employé au guichet donné, persiste la session
     * terminée dans la BD
     * 
     * @param deskNumber le numéro du guichet
     */
    public synchronized void employeDeconnecte(int deskNumber) {
        termineRequete(deskNumber);
        WorkSession terminee = deskManager.employeDeconnecte(deskNumber).orElseThrow();
        terminee.fermer();
        log.info("On enregistre la fermeture du guichet : {}", terminee);
        workSessionDAO.save(terminee);
    }

    /**
     * @return les requêtes en cours de service
     */
    public synchronized Collection<ServiceRequest> requetesEnCours() {
        return enCours.requetesEnCours();
    }

    private void dispatchRequest() {
        dispatcher.envoieRequeteAuGuichet().ifPresent(request -> {
            log.info("La requête va être servie {}", request);
            enCours.ajoute(request);
            deskManager.employeIndisponible(request.getGuichetAffecte());
            notifierService(request); // On met à jour les affichages
        });
    }

    private void termineRequete(int deskNumber) {
        Optional<ServiceRequest> active = enCours.termine(deskNumber);
        active.ifPresent(requete -> {
            requete.termineService();
            log.info("On enregistre la requête terminée : {}", requete);
            requestDao.save(requete);
            notifierService(requete); // On met à jour les affichages
        });
    }

    /**
     * Notifie les IHM (écrans et poste de travail 'employé')
     * quand une nouvelle requête est en cours de service ou terminée
     * @param request la requête
     */
    private void notifierService(ServiceRequest request) {
        publisher.convertAndSend("/topic/service", request);
    }
}
