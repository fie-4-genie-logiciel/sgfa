var stompClient = null;

function connect(callback) {
    var socket = new SockJS('/sgfa');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/service', function (payload) {
            var serviceRequest = JSON.parse(payload.body);
            console.log(serviceRequest);
            callback(serviceRequest);
        });
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    console.log("Disconnected");
}
