INSERT INTO EMPLOYEE(id, name, habilite_pro) VALUES 
    (1, 'Rémi Bastide', false), 
    (2, 'Elyes Lamine', true), 
    (3, 'Imen Megdiche', false),
    (4, 'Réjane Dalce', true), 
    (5, 'Adrien Defossez', false);
-- Pour faciliter l'écriture des requêtes SQL des statistiques
DROP ALIAS IF EXISTS trancheHoraire;
CREATE ALIAS trancheHoraire FOR "io.github.bastide.sgfa.sqlutil.SqlFunctions.trancheHoraire";